import React from 'react'
import { Route } from 'react-router-dom'
import Layout from './components/Layout'

import Home from './components/Home'
import Dashboard from './components/Dashboard'
import WidgetForm from './components/WidgetForm'
import DashboardManagement from './components/DashboardManagement'
import RegistrationForm from './components/RegistrationForm'
import LoginForm from './components/LoginForm'

function App() {
  return (
    <Layout>
      <Route exact path="/" component={Home} />
      <Route exact path="/dashboards" component={Dashboard} />
      <Route exact path="/dashboards/manage" component={DashboardManagement} />
      <Route exact path="/widgets/create" component={WidgetForm} />
      <Route exact path="/users/register" component={RegistrationForm} />
      <Route exact path="/users/login" component={LoginForm} />
    </Layout>
  )
}

export default App
