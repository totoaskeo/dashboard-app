import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Form, FormGroup, Button, Input, Label } from 'reactstrap'
import { actionCreators } from '../store/Users'

class RegistrationForm extends Component {
  constructor(props) {
    super(props)
    this.state = {
      username: '',
      email: '',
      password: '',
      confirmPassword: ''
    }
    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  handleChange(e) {
    const { name, value } = e.target
    this.setState({ [name]: value })
  }

  async handleSubmit(e) {
    e.preventDefault()
    if (this.state.password !== this.state.confirmPassword) {
      // TODO add validation messsages
      return
    }
    await this.props.registerUser(this.state)
  }

  render() {
    return (
      <Form>
        <FormGroup>
          <Label>Username</Label>
          <Input
            value={this.state.username}
            onChange={this.handleChange}
            type="text"
            name="username"
            required
          />
        </FormGroup>
        <FormGroup>
          <Label>Email</Label>
          <Input
            value={this.state.email}
            onChange={this.handleChange}
            type="email"
            name="email"
            required
          />
        </FormGroup>
        <FormGroup>
          <Label>Password</Label>
          <Input
            value={this.state.password}
            onChange={this.handleChange}
            type="password"
            name="password"
            required
          />
        </FormGroup>
        <FormGroup>
          <Label>Confirm password</Label>
          <Input
            value={this.state.confirmPassword}
            onChange={this.handleChange}
            type="password"
            name="confirmPassword"
            required
          />
        </FormGroup>
        <Button outline color="primary" onClick={this.handleSubmit}>
          Sign up
        </Button>
      </Form>
    )
  }
}

export default connect(
  null,
  dispatch => bindActionCreators(actionCreators, dispatch)
)(RegistrationForm)
