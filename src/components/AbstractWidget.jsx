import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { BarChart, Bar, Brush, Cell, CartesianGrid, ReferenceLine, ReferenceDot,
  XAxis, YAxis, Tooltip, Legend, ErrorBar, LabelList } from 'recharts';

export class AbstractWidget extends Component {
  render() {
    return (
      <div>
        <BarChart width={400} height={400}>
          <XAxis padding={{ left: 20, right: 100 }} type="number" dataKey="time" />
          <YAxis type="number" />
          <CartesianGrid horizontal={false} />
            <Tooltip />
            <Bar dataKey="uv" fill="#ff7300" maxBarSize={15} isAnimationActive={false} />
            <Bar dataKey="pv" fill="#387908" />
        </BarChart>
      </div>
    )
  }
}

AbstractWidget.propTypes = {
  widget: PropTypes.string
}

export default AbstractWidget;
