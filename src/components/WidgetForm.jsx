import React, { Component } from 'react';
import { Form, Input, Button, FormGroup, Label } from 'reactstrap';

class WidgetForm extends Component { 
  render () {
    return (
      <Form className="col-sm-12 col-md-6">
        <FormGroup>
          <Label>Тип</Label>
          <Input type="select"></Input>
        </FormGroup>
        <FormGroup>
          <Label>Тип графика</Label>
          <Input type="select"></Input>
        </FormGroup>
        <FormGroup>
          <Label>Цветовая схема</Label>
          <Input type="select"></Input>
        </FormGroup>
        <FormGroup check>
          <Input type="checkbox" id="showLegendCheck"></Input>
          <Label>Отображать легенду</Label>
        </FormGroup>
        <FormGroup>
          <Label>Место отображения легенды</Label>
          <Input type="select"></Input>
        </FormGroup>
        <Button>Сохранить</Button>
      </Form>
    )
  }
}

export default WidgetForm;
