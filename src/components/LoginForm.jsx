import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Form, Button, FormGroup, Input, Label } from 'reactstrap'
import { actionCreators } from '../store/Users'

class LoginForm extends Component {
  constructor (props) {
    super(props)
    this.state = {
      username: '',
      password: ''
    }
    this.handleChange = this.handleChange.bind(this)
  }

  handleChange (e) {
    const { name, value } = e.target
    this.setState({ [name]: value })
  }

  async handleSubmit (e) {
    e.preventDefault()
    await this.props.loginUser(this.state.username, this.state.password)
  }

  render () {
    return (
      <Form>
        <FormGroup>
          <Label>Username</Label>
          <Input value={this.state.username} onChange={this.handleChange} type="text" />
        </FormGroup>
        <FormGroup>
          <Label>Password</Label>
          <Input value={this.state.password} onChange={this.handleChange} type="password" />
        </FormGroup>
        <Button outline color="primary">Login</Button>
      </Form>
    )
  }
}

export default connect(
  null,
  dispatch => bindActionCreators(actionCreators, dispatch)
)(LoginForm)
