import React, { Fragment } from 'react';
import { Container } from 'reactstrap';
import TopNavBar from './TopNavBar';

export default props =>
  <Fragment>
    <TopNavBar></TopNavBar>
    <Container>
      {props.children}
    </Container>
  </Fragment>