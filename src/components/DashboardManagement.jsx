import React, { Component } from 'react'
import { Button } from 'reactstrap'
import CreateDashboardModal from './CreateDashboardModal'

class DashboardManagement extends Component {
  constructor(props) {
    super(props)
    this.state = {
    }
    this.handleCreateDashboard = this.handleCreateDashboard.bind(this)
  }

  render() {
    return (
      <>
        <div>
          <CreateDashboardModal></CreateDashboardModal>
        </div>
        {/* Dashboard previews here */}
      </>
    )
  }
}

export default DashboardManagement
