const initialState = { list: [] }

export const actionCreators = {
  createDashboard (dashboard) {
    const dashboards = JSON.parse(localStorage.getItem('dashboards'))
    dashboards.push(dashboard)
    localStorage.setItem('dashboards', JSON.stringify(dashboards))
  },
  getDashboards () {
    const dashboards = JSON.parse(localStorage.getItem('dashboards'))
    return dashboards
  }
}

export const reducer = (state, action) => {
  state = state || initialState

  return state
}
