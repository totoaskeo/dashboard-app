import * as actionTypes from './actionTypes'
import * as userService from '../services/userService'

const initialState = { user: {}, list: [], isLoading: false }

export const actionCreators = {
  registerUser: user => async dispatch => {
    dispatch({ type: actionTypes.REGISTER_USER_ATTEMPT })
    const result = userService.registerUser(user)
    if (result.err) {
      dispatch({ type: actionTypes.REGISTER_USER_FAILURE, err: result.err })
    } else {
      dispatch({ type: actionTypes.REGISTER_USER_SUCCESS })
    }
  },
  loginUser: (username, password) => async dispatch => {
    dispatch({ type: actionTypes.LOGIN_USER_ATTEMPT })
    const result = userService.loginUser(username, password)
    if (result.err) {
      dispatch({ type: actionTypes.LOGIN_USER_FAILURE, err: result.err })
    } else {
      dispatch({ type: actionTypes.LOGIN_USER_SUCCESS, user: result.user })
    }
  }
}

export const reducer = (state, action) => {
  state = state || initialState

  switch (action.type) {
    case actionTypes.REGISTER_USER_ATTEMPT:
    case actionTypes.LOGIN_USER_ATTEMPT:
      return { ...state, isLoading: true }

    case actionTypes.REGISTER_USER_SUCCESS:
      return { ...state, isLoading: false }

    case actionTypes.REGISTER_USER_FAILURE:
    case actionTypes.LOGIN_USER_FAILURE:
      return { ...state, isLoading: false, err: action.err }

    case actionTypes.LOGIN_USER_SUCCESS:
      return { ...state, isLoading: false, user: action.user }
      
    default:
      return state
  }
}
