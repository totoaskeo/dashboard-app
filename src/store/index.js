import { combineReducers, createStore, applyMiddleware, compose } from 'redux'
import thunkMiddleware from 'redux-thunk'
import * as Users from './Users'

export default function configureStore(preloadedState) {
  const reducers = {
    users: Users.reducer
  }

  const middleware = [thunkMiddleware]

  const rootReducer = combineReducers({
    ...reducers
  })

  const middlewareEnhancer = applyMiddleware(...middleware)
  const enhancers = []

  return createStore(
    rootReducer,
    preloadedState,
    compose(
      middlewareEnhancer,
      ...enhancers
    )
  )
}
