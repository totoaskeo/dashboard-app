export function registerUser(user) {
  let err
  const users = JSON.parse(localStorage.getItem('users')) || []
  users.push(user)
  localStorage.setItem('users', JSON.stringify(users))
  return { err }
}

export function loginUser(username, password) {
  let err
  const users = JSON.parse(localStorage.getItem('users')) || []
  const user = users.find(u => u.username === username)
  if (user.password !== password) {
    err = 'Wrong username or password'
  }
  localStorage.setItem('user', JSON.stringify(user))
  return { err, user }
}
